@echo off

"%JAVA_HOME%\bin\java" -jar tools\apktool.jar b -a tools\aapt.exe . GlobalDolbyEffect-unsigned.apk
if not %ERRORLEVEL == 0 (
 echo !!! Building failed !
 goto end
)

"%JAVA_HOME%\bin\java" -jar tools\signapk.jar tools\platform.x509.pem tools\platform.pk8 GlobalDolbyEffect-unsigned.apk GlobalDolbyEffect.apk
if not %ERRORLEVEL == 0 (
 echo !!! Signing failed !
 goto end
)
del /f "GlobalDolbyEffect-unsigned.apk

:end