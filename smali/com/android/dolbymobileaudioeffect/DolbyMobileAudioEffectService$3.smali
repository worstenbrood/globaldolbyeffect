.class Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;
.super Landroid/os/Handler;
.source "DolbyMobileAudioEffectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;


# direct methods
.method constructor <init>(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V
    .locals 0
    .parameter

    .prologue
    .line 1145
    iput-object p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .parameter "msg"

    .prologue
    const/4 v7, 0x1

    .line 1147
    iget v6, p1, Landroid/os/Message;->what:I

    sparse-switch v6, :sswitch_data_0

    .line 1240
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 1242
    :cond_0
    :goto_0
    return-void

    .line 1150
    :sswitch_0
    const-string v6, "DMService"

    const-string v7, "handling the EFFECT_ON_OFF_MSG message..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1151
    const/4 v3, 0x1

    .line 1152
    .local v3, isEffectOn:Z
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1154
    :try_start_0
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->audioEffectEnabled()Z
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v3

    .line 1155
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1156
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v6

    if-eq v3, v6, :cond_0

    .line 1158
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1160
    :try_start_1
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 1161
    .local v0, N:I
    const/4 v2, 0x0

    .local v2, i:I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 1163
    :try_start_2
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Landroid/media/dolby/IDolbyMobileServiceCallbacks;

    invoke-interface {v6, v3}, Landroid/media/dolby/IDolbyMobileServiceCallbacks;->onEffectOnChanged(Z)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    .line 1161
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1155
    .end local v0           #N:I
    .end local v2           #i:I
    :catchall_0
    move-exception v6

    :try_start_3
    monitor-exit v7
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v6

    .line 1169
    .restart local v0       #N:I
    .restart local v2       #i:I
    :cond_1
    :try_start_4
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1170
    monitor-exit v7
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1171
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z
    invoke-static {v6, v3}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3002(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)Z

    goto :goto_0

    .line 1170
    .end local v0           #N:I
    .end local v2           #i:I
    :catchall_1
    move-exception v6

    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v6

    .line 1176
    .end local v3           #isEffectOn:Z
    :sswitch_1
    const-string v6, "DMService"

    const-string v7, "handling the PRESET_MSG message..."

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1177
    const/4 v5, 0x0

    .local v5, presetCatetory:I
    const/4 v4, 0x0

    .line 1178
    .local v4, preset:I
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1180
    :try_start_6
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getAudioEffectPresetCategory()I
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v5

    .line 1181
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->currentAudioEffectPreset()I
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v4

    .line 1182
    monitor-exit v7
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1183
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3100(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v6

    if-eq v5, v6, :cond_3

    .line 1185
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1187
    :try_start_7
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    move-result v0

    .line 1188
    .restart local v0       #N:I
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_3
    if-ge v2, v0, :cond_2

    .line 1190
    :try_start_8
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Landroid/media/dolby/IDolbyMobileServiceCallbacks;

    invoke-interface {v6, v5, v4}, Landroid/media/dolby/IDolbyMobileServiceCallbacks;->onPresetChanged(II)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_2

    .line 1188
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1182
    .end local v0           #N:I
    .end local v2           #i:I
    :catchall_2
    move-exception v6

    :try_start_9
    monitor-exit v7
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    throw v6

    .line 1196
    .restart local v0       #N:I
    .restart local v2       #i:I
    :cond_2
    :try_start_a
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1197
    monitor-exit v7
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    .line 1198
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I
    invoke-static {v6, v5}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3102(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    .line 1199
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I
    invoke-static {v6, v4}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3202(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    goto/16 :goto_0

    .line 1197
    .end local v0           #N:I
    .end local v2           #i:I
    :catchall_3
    move-exception v6

    :try_start_b
    monitor-exit v7
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    throw v6

    .line 1201
    :cond_3
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v6

    if-eq v4, v6, :cond_0

    .line 1203
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 1205
    :try_start_c
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->beginBroadcast()I
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    move-result v0

    .line 1206
    .restart local v0       #N:I
    const/4 v2, 0x0

    .restart local v2       #i:I
    :goto_5
    if-ge v2, v0, :cond_4

    .line 1208
    :try_start_d
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6, v2}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v6

    check-cast v6, Landroid/media/dolby/IDolbyMobileServiceCallbacks;

    invoke-interface {v6, v5, v4}, Landroid/media/dolby/IDolbyMobileServiceCallbacks;->onPresetChanged(II)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_1

    .line 1206
    :goto_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 1214
    :cond_4
    :try_start_e
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v6, v6, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v6}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    .line 1215
    monitor-exit v7
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_4

    .line 1216
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I
    invoke-static {v6, v4}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3202(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    goto/16 :goto_0

    .line 1215
    .end local v0           #N:I
    .end local v2           #i:I
    :catchall_4
    move-exception v6

    :try_start_f
    monitor-exit v7
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_4

    throw v6

    .line 1220
    .end local v4           #preset:I
    .end local v5           #presetCatetory:I
    :sswitch_2
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->popDolbyNotification()V
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V

    goto/16 :goto_0

    .line 1224
    :sswitch_3
    iget v6, p1, Landroid/os/Message;->arg1:I

    if-ne v6, v7, :cond_6

    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v6

    if-nez v6, :cond_6

    .line 1225
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I
    invoke-static {v6, v7}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3302(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    .line 1227
    :try_start_10
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    move-result-object v6

    invoke-virtual {v6}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->getEffectOn()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->isNeedFiltered()Z
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_0

    move-result v6

    if-nez v6, :cond_0

    .line 1236
    :cond_5
    :goto_7
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->popDolbyNotification()V
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V

    goto/16 :goto_0

    .line 1230
    :catch_0
    move-exception v1

    .line 1231
    .local v1, e:Ljava/lang/Exception;
    const-string v6, "DolbyMobileAudioEffectService"

    const-string v7, "notifydolbystatus() failed."

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 1233
    .end local v1           #e:Ljava/lang/Exception;
    :cond_6
    iget v6, p1, Landroid/os/Message;->arg1:I

    if-nez v6, :cond_5

    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I
    invoke-static {v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v6

    if-ne v6, v7, :cond_5

    .line 1234
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    const/4 v7, 0x0

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I
    invoke-static {v6, v7}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$3302(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    goto :goto_7

    .line 1209
    .restart local v0       #N:I
    .restart local v2       #i:I
    .restart local v4       #preset:I
    .restart local v5       #presetCatetory:I
    :catch_1
    move-exception v6

    goto :goto_6

    .line 1191
    :catch_2
    move-exception v6

    goto/16 :goto_4

    .line 1164
    .end local v4           #preset:I
    .end local v5           #presetCatetory:I
    .restart local v3       #isEffectOn:Z
    :catch_3
    move-exception v6

    goto/16 :goto_2

    .line 1147
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x7d1 -> :sswitch_2
        0x7dd -> :sswitch_3
    .end sparse-switch
.end method
