.class Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;
.super Landroid/content/BroadcastReceiver;
.source "DolbyMobileAudioEffectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;


# direct methods
.method constructor <init>(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V
    .locals 0
    .parameter

    .prologue
    .line 267
    iput-object p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .parameter "context"
    .parameter "intent"

    .prologue
    .line 273
    :try_start_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 275
    .local v0, action:Ljava/lang/String;
    const-string v2, "DMService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "intentReceiver_.onReceive "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    const-string v2, "com.dolby.dm.srvcmd.checkstatus"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 279
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->validateDmEffect()Z
    invoke-static {v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    .line 295
    .end local v0           #action:Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 281
    .restart local v0       #action:Ljava/lang/String;
    :cond_1
    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 282
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    const-string v3, "state"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    #setter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I
    invoke-static {v2, v3}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$102(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    .line 286
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->popDolbyNotification()V
    invoke-static {v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 290
    .end local v0           #action:Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 292
    .local v1, ex:Ljava/lang/Exception;
    const-string v2, "DMService"

    const-string v3, "Exception found in DMService::onReceive()"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method
