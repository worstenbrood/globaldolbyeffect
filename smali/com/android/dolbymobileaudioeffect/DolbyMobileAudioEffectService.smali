.class public Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;
.super Landroid/app/Service;
.source "DolbyMobileAudioEffectService.java"


# instance fields
.field private intentReceiver_:Landroid/content/BroadcastReceiver;

.field private lastEffectOn:Z

.field private lastPreset:I

.field private lastPresetCategory:I

.field private final mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

.field private mBlackList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Landroid/media/dolby/IDolbyMobileServiceCallbacks;",
            ">;"
        }
    .end annotation
.end field

.field private mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

.field mDolbyMobilePresetCategory:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

.field private mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

.field public mDolbySharedPreferences:Landroid/content/SharedPreferences;

.field private final mEffectOnOnStartup:Z

.field private mGlobalAudioEffectPreset:I

.field private mGlobalAudioHeadsetByPass:Z

.field private mGlobalAudioSpeakerByPass:Z

.field private mGlobalDolbyEffectOn:Z

.field private mGlobalVideoEffectPreset:I

.field private mGlobalVideoHeadsetByPass:Z

.field private mGlobalVideoSpeakerByPass:Z

.field private final mHandler:Landroid/os/Handler;

.field private mIsHeadset:I

.field private mIsMediaFilePlaying:I

.field private mLastPresetCategory:I

.field private final mLockCallbacks:Ljava/lang/Object;

.field private final mLockDolbyContext:Ljava/lang/Object;

.field private final mSetEffectOnByEnable:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 157
    const-string v0, "dolbymobileaudioeffect_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 158
    const-string v0, "DMService"

    const-string v1, "libdolbymobileaudioeffect_jni.so library is successfully loaded"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 63
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 67
    iput-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mEffectOnOnStartup:Z

    .line 71
    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mSetEffectOnByEnable:Z

    .line 74
    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z

    .line 75
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I

    .line 76
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I

    .line 106
    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalDolbyEffectOn:Z

    .line 107
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I

    .line 108
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I

    .line 109
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioEffectPreset:I

    .line 110
    iput-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioHeadsetByPass:Z

    .line 111
    iput-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioSpeakerByPass:Z

    .line 112
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoEffectPreset:I

    .line 113
    iput-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoHeadsetByPass:Z

    .line 114
    iput-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoSpeakerByPass:Z

    .line 115
    iput v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLastPresetCategory:I

    .line 116
    iput-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    .line 117
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBlackList:Ljava/util/List;

    .line 122
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;

    .line 124
    iput-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    .line 126
    sget-object v0, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->MUSIC:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresetCategory:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 128
    iput-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    .line 132
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;

    .line 137
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 266
    new-instance v0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;

    invoke-direct {v0, p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$1;-><init>(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->intentReceiver_:Landroid/content/BroadcastReceiver;

    .line 619
    new-instance v0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;

    invoke-direct {v0, p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;-><init>(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    .line 1145
    new-instance v0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;

    invoke-direct {v0, p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$3;-><init>(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V

    iput-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->validateDmEffect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getAudioEffectPresetCategory()I

    move-result v0

    return v0
.end method

.method static synthetic access$102(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I

    return p1
.end method

.method static synthetic access$1100(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getNumAudioEffectPresets()I

    move-result v0

    return v0
.end method

.method static synthetic access$1200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getNumAudioEffectPresets(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)Ljava/lang/String;
    .locals 1
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getAudioEffectPresetName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->bypassAudioEffect(Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalDolbyEffectOn()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalDolbyEffectOn(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalAudioEffectPreset()I

    move-result v0

    return v0
.end method

.method static synthetic access$1900(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalAudioEffectPreset(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V
    .locals 0
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->popDolbyNotification()V

    return-void
.end method

.method static synthetic access$2000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalAudioEffectHeadsetByPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2100(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalAudioEffectHeadsetByPass(Z)V

    return-void
.end method

.method static synthetic access$2200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalAudioEffectSpeakerByPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalAudioEffectSpeakerByPass(Z)V

    return-void
.end method

.method static synthetic access$2400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalVideoEffectPreset()I

    move-result v0

    return v0
.end method

.method static synthetic access$2500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalVideoEffectPreset(I)V

    return-void
.end method

.method static synthetic access$2600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalVideoEffectHeadsetByPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalVideoEffectHeadsetByPass(Z)V

    return-void
.end method

.method static synthetic access$2800(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalVideoEffectSpeakerByPass()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalVideoEffectSpeakerByPass(Z)V

    return-void
.end method

.method static synthetic access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z

    return v0
.end method

.method static synthetic access$3002(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)Z
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    iput-boolean p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I

    return v0
.end method

.method static synthetic access$3102(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I

    return p1
.end method

.method static synthetic access$3200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I

    return v0
.end method

.method static synthetic access$3202(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I

    return p1
.end method

.method static synthetic access$3300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I

    return v0
.end method

.method static synthetic access$3302(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I

    return p1
.end method

.method static synthetic access$3400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->isNeedFiltered()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->audioEffectEnabled()Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->enableAudioEffect(Z)V

    return-void
.end method

.method static synthetic access$600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Landroid/os/Handler;
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I
    .locals 1
    .parameter "x0"

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->currentAudioEffectPreset()I

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->selectAudioEffectPreset(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V
    .locals 0
    .parameter "x0"
    .parameter "x1"

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setAudioEffectPresetCategory(I)V

    return-void
.end method

.method private audioEffectEnabled()Z
    .locals 3

    .prologue
    .line 926
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->validateDmEffect()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 928
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v1}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getEnabled()Z

    move-result v0

    .line 934
    :goto_0
    return v0

    .line 933
    :cond_0
    const-string v1, "DMService"

    const-string v2, "Dolby Mobile effect is not valid."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 934
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private bypassAudioEffect(Z)V
    .locals 2
    .parameter "aBypass"

    .prologue
    .line 987
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->validateDmEffect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 989
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v0, p1}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->setBypass(Z)I

    .line 996
    :goto_0
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setSystemProperties()V

    .line 997
    return-void

    .line 993
    :cond_0
    const-string v0, "DMService"

    const-string v1, "Dolby Mobile effect is not valid."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private createDolbyMobile(Landroid/content/Intent;)V
    .locals 10
    .parameter "callerIntent"

    .prologue
    .line 414
    const-string v6, "DMService"

    const-string v7, "createDolbyMobile()"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    const/4 v0, 0x0

    .line 417
    .local v0, audioSessionId:I
    const-string v1, ""

    .line 418
    .local v1, audioSessionPkgName:Ljava/lang/String;
    sget-object v5, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->MUSIC:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 420
    .local v5, presetCategory:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;
    if-eqz p1, :cond_2

    .line 422
    const-string v6, "DMService"

    const-string v7, "(callerIntent != null)"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 425
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    .line 426
    .local v4, intentExtras:Landroid/os/Bundle;
    if-eqz v4, :cond_2

    .line 430
    const-string v6, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 432
    const-string v6, "android.media.extra.AUDIO_SESSION"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 433
    const-string v6, "DMService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEffect.EXTRA_AUDIO_SESSION = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_0
    const-string v6, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 438
    const-string v6, "android.media.extra.PACKAGE_NAME"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 439
    const-string v6, "DMService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "AudioEffect.EXTRA_PACKAGE_NAME = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 442
    :cond_1
    const-string v6, "android.media.extra.CONTENT_TYPE"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 444
    const-string v6, "android.media.extra.CONTENT_TYPE"

    invoke-virtual {v4, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 446
    .local v2, callerMediaContentType:I
    packed-switch v2, :pswitch_data_0

    .line 471
    const-string v6, "DolbyMobileSystemInterface"

    const-string v7, "dolby.media.extra.AUDIO_CATEGORY = UNKNOWN (setting as MUSIC)"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    :goto_0
    const-string v6, "DMService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "DolbyMobilePreset.Category = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v5}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 482
    .end local v2           #callerMediaContentType:I
    .end local v4           #intentExtras:Landroid/os/Bundle;
    :cond_2
    :try_start_0
    iget-object v7, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;

    monitor-enter v7
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_4

    .line 484
    :try_start_1
    new-instance v6, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-direct {v6, v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;-><init>(I)V

    iput-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    .line 486
    invoke-virtual {v5}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->toInt()I

    move-result v6

    invoke-direct {p0, v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setAudioEffectPresetCategory(I)V

    .line 491
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->setBypass(Z)I

    .line 493
    const/4 v6, 0x0

    invoke-direct {p0, v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->enableAudioEffect(Z)V

    .line 502
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setSystemProperties()V

    .line 505
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    array-length v6, v6

    if-lez v6, :cond_3

    .line 507
    iget-object v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    iget-object v8, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    const/4 v9, 0x0

    aget-object v8, v8, v9

    invoke-virtual {v6, v8}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->applyPreset(Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;)V

    .line 515
    :cond_3
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z

    .line 516
    invoke-virtual {v5}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->toInt()I

    move-result v6

    iput v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I

    .line 517
    const/4 v6, 0x0

    iput v6, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I

    .line 518
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 540
    :goto_1
    return-void

    .line 450
    .restart local v2       #callerMediaContentType:I
    .restart local v4       #intentExtras:Landroid/os/Bundle;
    :pswitch_0
    sget-object v5, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->MUSIC:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 451
    goto :goto_0

    .line 455
    :pswitch_1
    sget-object v5, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->MOVIE:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 456
    goto :goto_0

    .line 460
    :pswitch_2
    sget-object v5, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->GAME:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 461
    goto :goto_0

    .line 465
    :pswitch_3
    sget-object v5, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->VOICE:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 466
    goto :goto_0

    .line 518
    .end local v2           #callerMediaContentType:I
    .end local v4           #intentExtras:Landroid/os/Bundle;
    :catchall_0
    move-exception v6

    :try_start_2
    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v6
    :try_end_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_3 .. :try_end_3} :catch_4

    .line 520
    :catch_0
    move-exception v3

    .line 522
    .local v3, ex:Ljava/lang/ClassNotFoundException;
    const-string v6, "DMService"

    const-string v7, "DolbyMobile() FAILED! ClassNotFoundException"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 524
    .end local v3           #ex:Ljava/lang/ClassNotFoundException;
    :catch_1
    move-exception v3

    .line 526
    .local v3, ex:Ljava/lang/InstantiationException;
    const-string v6, "DMService"

    const-string v7, "DolbyMobile() FAILED! InstantiationException"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 528
    .end local v3           #ex:Ljava/lang/InstantiationException;
    :catch_2
    move-exception v3

    .line 530
    .local v3, ex:Ljava/lang/IllegalAccessException;
    const-string v6, "DMService"

    const-string v7, "DolbyMobile() FAILED! IllegalAccessException"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 532
    .end local v3           #ex:Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v3

    .line 534
    .local v3, ex:Ljava/lang/NoSuchMethodException;
    const-string v6, "DMService"

    const-string v7, "DolbyMobile() FAILED! NoSuchMethodException"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 536
    .end local v3           #ex:Ljava/lang/NoSuchMethodException;
    :catch_4
    move-exception v3

    .line 538
    .local v3, ex:Ljava/lang/reflect/InvocationTargetException;
    const-string v6, "DMService"

    const-string v7, "DolbyMobile() FAILED! InvocationTargetException"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 446
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private currentAudioEffectPreset()I
    .locals 3

    .prologue
    .line 1001
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v2}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getCurrentPreset()Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    move-result-object v1

    .line 1003
    .local v1, preset:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;
    const/4 v0, 0x0

    .line 1004
    .local v0, i:I
    :goto_0
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1006
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    aget-object v2, v2, v0

    if-ne v1, v2, :cond_0

    .line 1011
    .end local v0           #i:I
    :goto_1
    return v0

    .line 1004
    .restart local v0       #i:I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1011
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private enableAudioEffect(Z)V
    .locals 3
    .parameter "aEnable"

    .prologue
    .line 941
    :try_start_0
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->validateDmEffect()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 943
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v1, p1}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->setEnabled(Z)I

    .line 950
    :goto_0
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setSystemProperties()V

    .line 954
    :goto_1
    return-void

    .line 947
    :cond_0
    const-string v1, "DMService"

    const-string v2, "Dolby Mobile effect is not valid."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 951
    :catch_0
    move-exception v0

    .line 952
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "DolbyMobileAudioEffectService"

    const-string v2, "enableAudioEffect() failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private getAudioEffectPresetCategory()I
    .locals 1

    .prologue
    .line 1044
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresetCategory:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    invoke-virtual {v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->toInt()I

    move-result v0

    return v0
.end method

.method private getAudioEffectPresetName(I)Ljava/lang/String;
    .locals 2
    .parameter "aPreset"

    .prologue
    .line 1065
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    array-length v1, v1

    if-gt v1, p1, :cond_0

    .line 1068
    const/4 v1, 0x0

    .line 1071
    :goto_0
    return-object v1

    .line 1070
    :cond_0
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    aget-object v0, v1, p1

    .line 1071
    .local v0, preset:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;
    invoke-virtual {v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getDmInChans()I
    .locals 1

    .prologue
    .line 598
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getTdasInChans()I

    move-result v0

    return v0
.end method

.method private getDmInMatrix()I
    .locals 1

    .prologue
    .line 608
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getTdasInMatrix()I

    move-result v0

    return v0
.end method

.method private getDmMsrMaxProfile()I
    .locals 1

    .prologue
    .line 613
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getMsrMaxProfile()I

    move-result v0

    return v0
.end method

.method private getGlobalAudioEffectHeadsetByPass()Z
    .locals 1

    .prologue
    .line 1278
    iget-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioHeadsetByPass:Z

    return v0
.end method

.method private getGlobalAudioEffectPreset()I
    .locals 1

    .prologue
    .line 1266
    iget v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioEffectPreset:I

    return v0
.end method

.method private getGlobalAudioEffectSpeakerByPass()Z
    .locals 1

    .prologue
    .line 1290
    iget-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioSpeakerByPass:Z

    return v0
.end method

.method private getGlobalDolbyEffectOn()Z
    .locals 1

    .prologue
    .line 1248
    iget-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalDolbyEffectOn:Z

    return v0
.end method

.method private getGlobalVideoEffectHeadsetByPass()Z
    .locals 1

    .prologue
    .line 1314
    iget-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoHeadsetByPass:Z

    return v0
.end method

.method private getGlobalVideoEffectPreset()I
    .locals 1

    .prologue
    .line 1302
    iget v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoEffectPreset:I

    return v0
.end method

.method private getGlobalVideoEffectSpeakerByPass()Z
    .locals 1

    .prologue
    .line 1326
    iget-boolean v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoSpeakerByPass:Z

    return v0
.end method

.method private getMobileSurroundEnable()Z
    .locals 1

    .prologue
    .line 603
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getMsrEnable()Z

    move-result v0

    return v0
.end method

.method private getNumAudioEffectPresets()I
    .locals 1

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    array-length v0, v0

    return v0
.end method

.method private getNumAudioEffectPresets(I)I
    .locals 4
    .parameter "aCategory"

    .prologue
    .line 1055
    :try_start_0
    invoke-static {p1}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->FromInt(I)Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    move-result-object v0

    .line 1056
    .local v0, category:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v2, v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getPresets(Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;)[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    move-result-object v2

    array-length v2, v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1059
    .end local v0           #category:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;
    :goto_0
    return v2

    .line 1057
    :catch_0
    move-exception v1

    .line 1058
    .local v1, e:Ljava/lang/Exception;
    const-string v2, "DolbyMobileAudioEffectService"

    const-string v3, "getNumAudioEffectPresets() failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1059
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isNeedFiltered()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1410
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1415
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 1416
    if-nez v0, :cond_0

    move v0, v1

    .line 1427
    :goto_0
    return v0

    .line 1419
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1420
    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0x64

    if-eq v3, v4, :cond_2

    iget v3, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0xc8

    if-ne v3, v4, :cond_1

    .line 1422
    :cond_2
    iget-object v3, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBlackList:Ljava/util/List;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1423
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 1427
    goto :goto_0
.end method

.method private isWeatherPaper()Z
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1431
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1432
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 1433
    if-nez v0, :cond_1

    .line 1451
    :cond_0
    :goto_0
    return v2

    .line 1438
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 1439
    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v7, 0xc8

    if-ne v6, v7, :cond_2

    iget-object v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string v7, "com.huawei.android.WeatherWallpaper"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    move v3, v4

    .line 1443
    :cond_2
    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v7, 0x64

    if-ne v6, v7, :cond_5

    iget-object v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string v7, "com.huawei.android.launcher"

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    const-string v6, "com.android.launcher2"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v4

    :goto_2
    move v1, v0

    .line 1445
    goto :goto_1

    .line 1448
    :cond_4
    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    move v2, v4

    .line 1449
    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method private loadBlackList()V
    .locals 3

    .prologue
    .line 1395
    invoke-virtual {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f030001

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getXml(I)Landroid/content/res/XmlResourceParser;

    move-result-object v0

    .line 1397
    :goto_0
    :try_start_0
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_1

    .line 1398
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->getEventType()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "blockpackage"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1400
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBlackList:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Landroid/content/res/XmlResourceParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1402
    :cond_0
    invoke-interface {v0}, Landroid/content/res/XmlResourceParser;->next()I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1404
    :catch_0
    move-exception v0

    .line 1405
    const-string v0, "DolbyMobileAudioEffectService"

    const-string v1, "loadBlackList() failed."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1407
    :cond_1
    return-void
.end method

.method private loadGlobalParameters()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1376
    const-string v1, "GlobalDolbyEffect"

    invoke-virtual {p0, v1, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    .line 1377
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    if-nez v1, :cond_0

    .line 1392
    :goto_0
    return-void

    .line 1381
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "dolby_effect_on"

    const/4 v3, 0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalDolbyEffectOn:Z

    .line 1382
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "audio_headset_preset"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioEffectPreset:I

    .line 1383
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "audio_headset_bypass"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioHeadsetByPass:Z

    .line 1384
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "audio_speaker_bypass"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioSpeakerByPass:Z

    .line 1385
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "video_headset_preset"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoEffectPreset:I

    .line 1386
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "video_headset_bypass"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoHeadsetByPass:Z

    .line 1387
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "video_speaker_bypass"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoSpeakerByPass:Z

    .line 1388
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    const-string v2, "last_preset_category"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLastPresetCategory:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1389
    :catch_0
    move-exception v0

    .line 1390
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "DolbyMobileAudioEffectService"

    const-string v2, "Load global parameters failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private popDolbyNotification()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    .line 165
    :try_start_0
    const-string v7, "notification"

    invoke-virtual {p0, v7}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/NotificationManager;

    .line 166
    .local v4, mNotificationManager:Landroid/app/NotificationManager;
    const/16 v7, 0x54d

    invoke-virtual {v4, v7}, Landroid/app/NotificationManager;->cancel(I)V

    .line 168
    iget v7, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I

    if-ne v7, v8, :cond_0

    #iget v7, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsMediaFilePlaying:I
    #if-ne v7, v8, :cond_0

    .line 170
    iget-object v7, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    invoke-virtual {v7}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->getPresetCategory()I

    move-result v0

    .line 171
    .local v0, category:I
    if-nez v0, :cond_1

    .line 172
    const v7, 0x7f060040

    invoke-virtual {p0, v7}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 176
    .local v2, contentText:Ljava/lang/CharSequence;
    :goto_0
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.huawei.android.globaldolbyeffect.DolbyEffectAlertDialog"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 177
    .local v6, notificationIntent:Landroid/content/Intent;
    const/high16 v7, 0x1000

    invoke-virtual {v6, v7}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 178
    const-string v7, "dolbycategory"

    invoke-virtual {v6, v7, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 179
    const/4 v7, 0x0

    const/high16 v8, 0x800

    invoke-static {p0, v7, v6, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 181
    .local v1, contentIntent:Landroid/app/PendingIntent;
    new-instance v7, Landroid/app/Notification$Builder;

    invoke-direct {v7, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    const v8, 0x7f060042

    invoke-virtual {p0, v8}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f020002

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v7

    const v8, 0x7f020002

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7, v1}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v7

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v5

    .line 190
    .local v5, notification:Landroid/app/Notification;
    const/16 v7, 0x54d

    invoke-virtual {v4, v7, v5}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 195
    .end local v0           #category:I
    .end local v1           #contentIntent:Landroid/app/PendingIntent;
    .end local v2           #contentText:Ljava/lang/CharSequence;
    .end local v4           #mNotificationManager:Landroid/app/NotificationManager;
    .end local v5           #notification:Landroid/app/Notification;
    .end local v6           #notificationIntent:Landroid/content/Intent;
    :cond_0
    :goto_1
    return-void

    .line 174
    .restart local v0       #category:I
    .restart local v4       #mNotificationManager:Landroid/app/NotificationManager;
    :cond_1
    const v7, 0x7f060041

    invoke-virtual {p0, v7}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .restart local v2       #contentText:Ljava/lang/CharSequence;
    goto :goto_0

    .line 192
    .end local v0           #category:I
    .end local v2           #contentText:Ljava/lang/CharSequence;
    .end local v4           #mNotificationManager:Landroid/app/NotificationManager;
    :catch_0
    move-exception v3

    .line 193
    .local v3, e:Ljava/lang/Exception;
    const-string v7, "DolbyMobileAudioEffectService"

    const-string v8, "popDolbyNotification() failed."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method private recreateDmEffect()Z
    .locals 5

    .prologue
    .line 1107
    const/4 v1, 0x0

    .line 1110
    .local v1, ret:Z
    :try_start_0
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    if-eqz v2, :cond_0

    .line 1112
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v2}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->release()V

    .line 1114
    :cond_0
    new-instance v2, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;-><init>(I)V

    iput-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    .line 1116
    const-string v2, "DMService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "recreateDmEffect(): lastEffectOn = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->setBypass(Z)I

    .line 1121
    iget-boolean v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastEffectOn:Z

    invoke-direct {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->enableAudioEffect(Z)V

    .line 1129
    iget v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPresetCategory:I

    invoke-direct {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setAudioEffectPresetCategory(I)V

    .line 1130
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    iget-object v3, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    iget v4, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->lastPreset:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->applyPreset(Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1131
    const/4 v1, 0x1

    .line 1138
    :goto_0
    return v1

    .line 1133
    :catch_0
    move-exception v0

    .line 1135
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "DMService"

    const-string v3, "Exception in recreateDmEffect."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1136
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static native registerDolby(Ljava/lang/Object;)V
.end method

.method private saveDolbySharedPreferences(Ljava/lang/String;I)V
    .locals 4
    .parameter "key"
    .parameter "value"

    .prologue
    .line 1350
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    .line 1360
    :goto_0
    return-void

    .line 1353
    :cond_0
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1355
    .local v1, edit:Landroid/content/SharedPreferences$Editor;
    :try_start_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1356
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1357
    :catch_0
    move-exception v0

    .line 1358
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "DolbyMobileAudioEffectService"

    const-string v3, "UnsupportedOperationException."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private saveDolbySharedPreferences(Ljava/lang/String;Z)V
    .locals 4
    .parameter "key"
    .parameter "value"

    .prologue
    .line 1363
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    if-nez v2, :cond_0

    .line 1373
    :goto_0
    return-void

    .line 1366
    :cond_0
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbySharedPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1368
    .local v1, edit:Landroid/content/SharedPreferences$Editor;
    :try_start_0
    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 1369
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1370
    :catch_0
    move-exception v0

    .line 1371
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "DolbyMobileAudioEffectService"

    const-string v3, "UnsupportedOperationException."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private selectAudioEffectPreset(I)V
    .locals 3
    .parameter "aPreset"

    .prologue
    .line 1016
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->validateDmEffect()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1018
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    array-length v1, v1

    if-gt v1, p1, :cond_0

    .line 1030
    :goto_0
    return-void

    .line 1023
    :cond_0
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    aget-object v0, v1, p1

    .line 1024
    .local v0, preset:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v1, v0}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->applyPreset(Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;)V

    goto :goto_0

    .line 1028
    .end local v0           #preset:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;
    :cond_1
    const-string v1, "DMService"

    const-string v2, "Dolby Mobile effect is not valid."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setAsForegroundService()V
    .locals 2

    .prologue
    .line 1455
    const/4 v0, 0x1

    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->startForeground(ILandroid/app/Notification;)V

    .line 1456
    return-void
.end method

.method private setAudioEffectPresetCategory(I)V
    .locals 3
    .parameter "aCategory"

    .prologue
    .line 1035
    :try_start_0
    invoke-static {p1}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;->FromInt(I)Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresetCategory:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    .line 1036
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresetCategory:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;

    invoke-virtual {v1, v2}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->getPresets(Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset$Category;)[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;

    move-result-object v1

    iput-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobilePresets:[Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobilePreset;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1040
    :goto_0
    return-void

    .line 1037
    :catch_0
    move-exception v0

    .line 1038
    .local v0, e:Ljava/lang/Exception;
    const-string v1, "DolbyMobileAudioEffectService"

    const-string v2, "setAudioEffectPresetCategory() failed."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setDmMsrUpmixerSystemProperty()V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 573
    const-string v7, "DMService"

    const-string v8, "setDmMsrUpmixerSystemProperty()"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 574
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getMobileSurroundEnable()Z

    move-result v3

    .line 575
    .local v3, dmMsrEnable:Z
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getDmInChans()I

    move-result v1

    .line 576
    .local v1, dmInChans:I
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getDmInMatrix()I

    move-result v2

    .line 577
    .local v2, dmInMatrix:I
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getDmMsrMaxProfile()I

    move-result v4

    .line 579
    .local v4, dmMsrMaxProfile:I
    const-string v0, "off"

    .line 580
    .local v0, curMsrUpmixerState:Ljava/lang/String;
    if-ne v3, v6, :cond_1

    .line 582
    const/4 v7, 0x4

    if-ne v2, v7, :cond_1

    .line 584
    const/4 v7, 0x2

    if-ne v1, v7, :cond_2

    move v7, v6

    :goto_0
    const/16 v8, 0x11

    if-ne v1, v8, :cond_0

    move v5, v6

    :cond_0
    or-int/2addr v5, v7

    if-eqz v5, :cond_1

    .line 586
    if-lt v4, v6, :cond_1

    .line 588
    const-string v0, "on"

    .line 593
    :cond_1
    const-string v5, "dolby.dm.msrupmixer.state"

    invoke-static {v5, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 594
    return-void

    :cond_2
    move v7, v5

    .line 584
    goto :goto_0
.end method

.method private setDmStateSystemProperty()V
    .locals 4

    .prologue
    .line 551
    const-string v2, "DMService"

    const-string v3, "setDmStateSystemProperty()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    const-string v0, "off"

    .line 555
    .local v0, curDmState:Ljava/lang/String;
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->audioEffectEnabled()Z

    move-result v1

    .line 556
    .local v1, enable:Z
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    const-string v0, "on"

    .line 568
    :goto_0
    const-string v2, "dolby.dm.state"

    invoke-static {v2, v0}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    .line 569
    return-void

    .line 556
    :cond_0
    const-string v0, "off"

    goto :goto_0
.end method

.method private setGlobalAudioEffectHeadsetByPass(Z)V
    .locals 1
    .parameter "byPass"

    .prologue
    .line 1282
    iput-boolean p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioHeadsetByPass:Z

    .line 1283
    const-string v0, "audio_headset_bypass"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;Z)V

    .line 1287
    return-void
.end method

.method private setGlobalAudioEffectPreset(I)V
    .locals 1
    .parameter "preset"

    .prologue
    .line 1270
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioEffectPreset:I

    .line 1271
    const-string v0, "audio_headset_preset"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;I)V

    .line 1275
    return-void
.end method

.method private setGlobalAudioEffectSpeakerByPass(Z)V
    .locals 1
    .parameter "byPass"

    .prologue
    .line 1294
    iput-boolean p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioSpeakerByPass:Z

    .line 1295
    const-string v0, "audio_speaker_bypass"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;Z)V

    .line 1299
    return-void
.end method

.method private setGlobalDolbyEffectOn(Z)V
    .locals 2
    .parameter "on"

    .prologue
    .line 1252
    iput-boolean p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalDolbyEffectOn:Z

    .line 1253
    const-string v1, "dolby_effect_on"

    invoke-direct {p0, v1, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;Z)V

    .line 1254
    if-eqz p1, :cond_0

    .line 1255
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->popDolbyNotification()V

    .line 1263
    :goto_0
    return-void

    .line 1257
    :cond_0
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1258
    .local v0, mNotificationManager:Landroid/app/NotificationManager;
    const/16 v1, 0x54d

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method

.method private setGlobalVideoEffectHeadsetByPass(Z)V
    .locals 1
    .parameter "byPass"

    .prologue
    .line 1318
    iput-boolean p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoHeadsetByPass:Z

    .line 1319
    const-string v0, "video_headset_bypass"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;Z)V

    .line 1323
    return-void
.end method

.method private setGlobalVideoEffectPreset(I)V
    .locals 1
    .parameter "preset"

    .prologue
    .line 1306
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoEffectPreset:I

    .line 1307
    const-string v0, "video_headset_preset"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;I)V

    .line 1311
    return-void
.end method

.method private setGlobalVideoEffectSpeakerByPass(Z)V
    .locals 1
    .parameter "byPass"

    .prologue
    .line 1330
    iput-boolean p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoSpeakerByPass:Z

    .line 1331
    const-string v0, "video_speaker_bypass"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;Z)V

    .line 1335
    return-void
.end method

.method private setLastPresetCategory(I)V
    .locals 1
    .parameter "category"

    .prologue
    .line 1342
    iput p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLastPresetCategory:I

    .line 1343
    const-string v0, "last_preset_category"

    invoke-direct {p0, v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->saveDolbySharedPreferences(Ljava/lang/String;I)V

    .line 1347
    return-void
.end method

.method private setSystemProperties()V
    .locals 2

    .prologue
    .line 544
    const-string v0, "DMService"

    const-string v1, "setSystemProperties()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setDmStateSystemProperty()V

    .line 546
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setDmMsrUpmixerSystemProperty()V

    .line 547
    return-void
.end method

.method private validateDmEffect()Z
    .locals 4

    .prologue
    .line 1081
    const/4 v1, 0x0

    .line 1083
    .local v1, ret:Z
    :try_start_0
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    if-eqz v2, :cond_0

    .line 1084
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    invoke-virtual {v2}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->hasControl()Z

    move-result v1

    .line 1087
    :cond_0
    if-nez v1, :cond_1

    .line 1089
    const-string v2, "DMService"

    const-string v3, "Cannot control the DmEffect, trying to recreate..."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1090
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->recreateDmEffect()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 1096
    :cond_1
    :goto_0
    return v1

    .line 1092
    :catch_0
    move-exception v0

    .line 1093
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "DolbyMobileAudioEffectService"

    const-string v3, "validateDmEffect() failed."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public changedolbymode(I)Z
    .locals 6
    .parameter "mode"

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 208
    if-nez p1, :cond_6

    .line 209
    :try_start_0
    iget-object v4, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    invoke-virtual {v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->getEffectOn()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->isNeedFiltered()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v2

    .line 212
    :cond_1
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    invoke-virtual {v2, p1}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setPresetCategory(I)V

    .line 213
    iget v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I

    if-ne v2, v3, :cond_4

    .line 214
    iget-boolean v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioHeadsetByPass:Z

    if-eqz v2, :cond_3

    .line 215
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    .line 228
    :goto_1
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setLastPresetCategory(I)V

    .line 251
    :cond_2
    :goto_2
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 252
    .local v1, msg:Landroid/os/Message;
    const/16 v2, 0x7d1

    iput v2, v1, Landroid/os/Message;->what:I

    .line 253
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .end local v1           #msg:Landroid/os/Message;
    :goto_3
    move v2, v3

    .line 257
    goto :goto_0

    .line 217
    :cond_3
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    .line 218
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    iget v4, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioEffectPreset:I

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->selectPreset(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 254
    :catch_0
    move-exception v0

    .line 255
    .local v0, e:Ljava/lang/Exception;
    const-string v2, "DolbyMobileAudioEffectService"

    const-string v4, "changedolbymode() FAILED."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 221
    .end local v0           #e:Ljava/lang/Exception;
    :cond_4
    :try_start_1
    iget-boolean v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalAudioSpeakerByPass:Z

    if-eqz v2, :cond_5

    .line 222
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    goto :goto_1

    .line 224
    :cond_5
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->selectPreset(I)V

    .line 225
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    goto :goto_1

    .line 229
    :cond_6
    if-ne p1, v3, :cond_2

    .line 230
    iget-object v4, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    invoke-virtual {v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->getEffectOn()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->isNeedFiltered()Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->isWeatherPaper()Z

    move-result v4

    if-nez v4, :cond_0

    .line 233
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    invoke-virtual {v2, p1}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setPresetCategory(I)V

    .line 234
    iget v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I

    if-ne v2, v3, :cond_8

    .line 235
    iget-boolean v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoHeadsetByPass:Z

    if-eqz v2, :cond_7

    .line 236
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    .line 249
    :goto_4
    invoke-direct {p0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setLastPresetCategory(I)V

    goto :goto_2

    .line 238
    :cond_7
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    .line 239
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    iget v4, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoEffectPreset:I

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->selectPreset(I)V

    goto :goto_4

    .line 242
    :cond_8
    iget-boolean v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalVideoSpeakerByPass:Z

    if-eqz v2, :cond_9

    .line 243
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    goto :goto_4

    .line 245
    :cond_9
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->setEffectByPass(Z)V

    .line 246
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;->selectPreset(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_4
.end method

.method public notifydolbystatus(I)V
    .locals 2
    .parameter "status"

    .prologue
    .line 199
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 200
    .local v0, msg:Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->arg1:I

    .line 201
    const/16 v1, 0x7dd

    iput v1, v0, Landroid/os/Message;->what:I

    .line 202
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 204
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .parameter

    .prologue
    .line 391
    const-string v0, "DMService"

    const-string v1, "DolbyMobileAudioEffectService.onBind()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 393
    const-class v0, Landroid/media/dolby/IDolbyMobileSystemInterface;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mBinder:Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;

    .line 403
    :goto_0
    return-object v0

    .line 402
    :cond_0
    const-string v0, "DMService"

    const-string v1, "/DolbyMobileAudioEffectService.onBind() - return null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 304
    const-string v2, "DMService"

    const-string v3, "DolbyMobileAudioEffectService.onCreate()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->createDolbyMobile(Landroid/content/Intent;)V

    .line 310
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 311
    .local v1, commandFilter:Landroid/content/IntentFilter;
    const-string v2, "com.dolby.dm.srvcmd.checkstatus"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 313
    const-string v2, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 317
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->intentReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 318
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->loadGlobalParameters()V

    .line 319
    invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->loadBlackList()V

    .line 320
    invoke-static {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->registerDolby(Ljava/lang/Object;)V

    .line 322
    iget-boolean v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mGlobalDolbyEffectOn:Z

    invoke-direct {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->enableAudioEffect(Z)V

    .line 325
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 326
    .local v0, audioManager:Landroid/media/AudioManager;
    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mIsHeadset:I

    .line 329
    iget v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLastPresetCategory:I

    invoke-virtual {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->changedolbymode(I)Z

    #.line 332
    #invoke-direct {p0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setAsForegroundService()V

    .line 333
    return-void

    .line 326
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 341
    const-string v2, "DMService"

    const-string v3, "DolbyMobileAudioEffectService.onDestroy()"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    iget-object v3, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;

    monitor-enter v3

    .line 346
    :try_start_0
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mDolbyMobile:Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/media/audiofx/dolbymobileaudioeffect/DolbyMobileSystem;->setEnabled(Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :goto_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352
    iget-object v3, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;

    monitor-enter v3

    .line 355
    :try_start_2
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v2}, Landroid/os/RemoteCallbackList;->kill()V

    .line 356
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 359
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    invoke-virtual {v2, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 360
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeMessages(I)V

    .line 363
    const v2, 0x7f06003a

    invoke-static {p0, v2, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 365
    iget-object v2, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->intentReceiver_:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 367
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 368
    .local v1, mNotificationManager:Landroid/app/NotificationManager;
    const/16 v2, 0x54d

    invoke-virtual {v1, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 369
    invoke-virtual {p0, v6}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->stopForeground(Z)V

    .line 370
    return-void

    .line 347
    .end local v1           #mNotificationManager:Landroid/app/NotificationManager;
    :catch_0
    move-exception v0

    .line 348
    .local v0, e:Ljava/lang/Exception;
    :try_start_3
    const-string v2, "DolbyMobileAudioEffectService"

    const-string v4, "onDestroy() failed."

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 350
    .end local v0           #e:Ljava/lang/Exception;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2

    .line 356
    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .parameter "callerIntent"
    .parameter "flags"
    .parameter "startId"

    .prologue
    .line 378
    const-string v0, "DMService"

    const-string v1, "DolbyMobileAudioEffectService.onStartCommand()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    const/4 v0, 0x1

    return v0
.end method
