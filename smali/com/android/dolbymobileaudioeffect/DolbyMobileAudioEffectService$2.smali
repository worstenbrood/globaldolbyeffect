.class Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;
.super Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;
.source "DolbyMobileAudioEffectService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;


# direct methods
.method constructor <init>(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)V
    .locals 0
    .parameter

    .prologue
    .line 620
    iput-object p1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    invoke-direct {p0}, Landroid/media/dolby/IDolbyMobileSystemInterface$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public currentPreset()I
    .locals 2

    .prologue
    .line 670
    const-string v0, "DMService"

    const-string v1, "IDolbyMobileSystemInterface.currentPreset()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 674
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->currentAudioEffectPreset()I
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 675
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getEffectOn()Z
    .locals 2

    .prologue
    .line 629
    const-string v0, "DMService"

    const-string v1, "IDolbyMobileSystemInterface.getEffectOn()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 631
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 634
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->audioEffectEnabled()Z
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 637
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalAudioHeadsetByPass()Z
    .locals 2

    .prologue
    .line 853
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 855
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalAudioEffectHeadsetByPass()Z
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 856
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalAudioPreset()I
    .locals 2

    .prologue
    .line 839
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 841
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalAudioEffectPreset()I
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1800(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 842
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalAudioSpeakerByPass()Z
    .locals 2

    .prologue
    .line 867
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 869
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalAudioEffectSpeakerByPass()Z
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 870
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalEffectOn()Z
    .locals 2

    .prologue
    .line 825
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 827
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalDolbyEffectOn()Z
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 828
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalVideoHeadsetByPass()Z
    .locals 2

    .prologue
    .line 895
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 897
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalVideoEffectHeadsetByPass()Z
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 898
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalVideoPreset()I
    .locals 2

    .prologue
    .line 881
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 883
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalVideoEffectPreset()I
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 884
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getGlobalVideoSpeakerByPass()Z
    .locals 2

    .prologue
    .line 909
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 911
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getGlobalVideoEffectSpeakerByPass()Z
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2800(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 912
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public getPresetCategory()I
    .locals 2

    .prologue
    .line 726
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 728
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getAudioEffectPresetCategory()I
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1000(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v0

    monitor-exit v1

    return v0

    .line 729
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public numPresets()I
    .locals 3

    .prologue
    .line 741
    const-string v1, "DMService"

    const-string v2, "IDolbyMobileSystemInterface.numPresets()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 745
    :try_start_0
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getNumAudioEffectPresets()I
    invoke-static {v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1100(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)I

    move-result v0

    .line 746
    .local v0, numPresets:I
    monitor-exit v2

    return v0

    .line 747
    .end local v0           #numPresets:I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public numPresetsPerCategory(I)I
    .locals 4
    .parameter "aCategory"

    .prologue
    .line 758
    const-string v1, "DMService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "IDolbyMobileSystemInterface.numPresetsPerCategory("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 760
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 762
    :try_start_0
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getNumAudioEffectPresets(I)I
    invoke-static {v1, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1200(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)I

    move-result v0

    .line 763
    .local v0, numPresets:I
    monitor-exit v2

    return v0

    .line 764
    .end local v0           #numPresets:I
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public presetName(I)Ljava/lang/String;
    .locals 5
    .parameter "aPreset"

    .prologue
    .line 775
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 777
    :try_start_0
    iget-object v1, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->getAudioEffectPresetName(I)Ljava/lang/String;
    invoke-static {v1, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)Ljava/lang/String;

    move-result-object v0

    .line 778
    .local v0, name:Ljava/lang/String;
    const-string v1, "DMService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "DolbyMobileAudioEffectService.presetName("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    monitor-exit v2

    return-object v0

    .line 780
    .end local v0           #name:Ljava/lang/String;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public registerCallback(Landroid/media/dolby/IDolbyMobileServiceCallbacks;)V
    .locals 2
    .parameter "cb"

    .prologue
    .line 790
    if-eqz p1, :cond_0

    .line 792
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 794
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v0, v0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 795
    monitor-exit v1

    .line 797
    :cond_0
    return-void

    .line 795
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public selectPreset(I)V
    .locals 3
    .parameter "aPreset"

    .prologue
    .line 688
    const-string v0, "DMService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IDolbyMobileSystemInterface.selectPreset("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 690
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 692
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->selectAudioEffectPreset(I)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$800(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V

    .line 694
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 695
    monitor-exit v1

    .line 696
    return-void

    .line 695
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setEffectByPass(Z)V
    .locals 2
    .parameter "byPass"

    .prologue
    .line 819
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 820
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->bypassAudioEffect(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 821
    monitor-exit v1

    .line 822
    return-void

    .line 821
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setEffectOn(Z)V
    .locals 3
    .parameter "on"

    .prologue
    .line 648
    const-string v0, "DMService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IDolbyMobileSystemInterface.setEffectOn("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 650
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 653
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->enableAudioEffect(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 658
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$600(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Landroid/os/Handler;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 659
    monitor-exit v1

    .line 660
    return-void

    .line 659
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalAudioHeadsetByPass(Z)V
    .locals 2
    .parameter "byPass"

    .prologue
    .line 860
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 862
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalAudioEffectHeadsetByPass(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2100(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 863
    monitor-exit v1

    .line 864
    return-void

    .line 863
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalAudioPreset(I)V
    .locals 2
    .parameter "preset"

    .prologue
    .line 846
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 848
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalAudioEffectPreset(I)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1900(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V

    .line 849
    monitor-exit v1

    .line 850
    return-void

    .line 849
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalAudioSpeakerByPass(Z)V
    .locals 2
    .parameter "byPass"

    .prologue
    .line 874
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 876
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalAudioEffectSpeakerByPass(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 877
    monitor-exit v1

    .line 878
    return-void

    .line 877
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalEffectOn(Z)V
    .locals 2
    .parameter "on"

    .prologue
    .line 832
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 834
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalDolbyEffectOn(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 835
    monitor-exit v1

    .line 836
    return-void

    .line 835
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalVideoHeadsetByPass(Z)V
    .locals 2
    .parameter "byPass"

    .prologue
    .line 902
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 904
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalVideoEffectHeadsetByPass(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2700(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 905
    monitor-exit v1

    .line 906
    return-void

    .line 905
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalVideoPreset(I)V
    .locals 2
    .parameter "preset"

    .prologue
    .line 888
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 890
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalVideoEffectPreset(I)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2500(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V

    .line 891
    monitor-exit v1

    .line 892
    return-void

    .line 891
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setGlobalVideoSpeakerByPass(Z)V
    .locals 2
    .parameter "byPass"

    .prologue
    .line 916
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 918
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setGlobalVideoEffectSpeakerByPass(Z)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$2900(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;Z)V

    .line 919
    monitor-exit v1

    .line 920
    return-void

    .line 919
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setPresetCategory(I)V
    .locals 3
    .parameter "aCategory"

    .prologue
    .line 708
    const-string v0, "DMService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IDolbyMobileSystemInterface.setPresetCategory("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 710
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockDolbyContext:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$300(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 712
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #calls: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->setAudioEffectPresetCategory(I)V
    invoke-static {v0, p1}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$900(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;I)V

    .line 713
    monitor-exit v1

    .line 714
    return-void

    .line 713
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unregisterCallback(Landroid/media/dolby/IDolbyMobileServiceCallbacks;)V
    .locals 2
    .parameter "cb"

    .prologue
    .line 807
    if-eqz p1, :cond_0

    .line 809
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    #getter for: Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mLockCallbacks:Ljava/lang/Object;
    invoke-static {v0}, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->access$1400(Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 811
    :try_start_0
    iget-object v0, p0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService$2;->this$0:Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;

    iget-object v0, v0, Lcom/android/dolbymobileaudioeffect/DolbyMobileAudioEffectService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 812
    monitor-exit v1

    .line 814
    :cond_0
    return-void

    .line 812
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
